#include <stdio.h>

int main(int argc, char *argv[]){
	
	int i;
	for (i = 1; i <= 101; i++)
	{
		if (i % 3 == 0  && i % 5 == 0)
		{
			printf("%d FizzBuzz\n", i);
		}
		if (i % 3 == 0)
		{
			printf("%d Fizz\n", i);
		}
		
		if (i % 5 == 0)
		
		{
			printf("%d Buzz\n", i);
		}
}	
	
	return 0;
}
